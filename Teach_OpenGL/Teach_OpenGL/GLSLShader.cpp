#include "GLSLShader.h"


GLSLShader::GLSLShader() 
{
	linked = false;
	ShaderProgram = 0;
}

GLSLShader::~GLSLShader() 
{
	if (ShaderProgram == 0) return;
	GLint numShaders = 0;
	glGetProgramiv(ShaderProgram, GL_ATTACHED_SHADERS, &numShaders);
	GLuint* shaderNames = new GLuint[numShaders];
	glGetAttachedShaders(ShaderProgram, numShaders, NULL, shaderNames);
	for (int i = 0; i < numShaders; ++i)
		glDeleteShader(shaderNames[i]);
	glDeleteProgram(ShaderProgram);
	delete[] shaderNames;
}

void GLSLShader::CompileShader(const char* shader_code, int Shader_Type)
{
	if (ShaderProgram <= 0) 
	{
		ShaderProgram = glCreateProgram();
		if (ShaderProgram == 0) 
		{
			throw GLSLShaderExcepton("Creating Shader Program fail");
		}
	}
	GLuint shader = glCreateShader(Shader_Type);
	glShaderSource(shader, 1, &shader_code, NULL);
	glCompileShader(shader);
	int result;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) 
	{
		int length = 0;
		std::string logString;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
		if (length > 0) 
		{
			char* c_log = new char[length];
			int written = 0;
			glGetShaderInfoLog(shader, length, &written, c_log);
			logString = c_log;
			delete[] c_log;
		}
		throw GLSLShaderExcepton("Shader compilation fail. " + logString);
	}
	else 
	{
		glAttachShader(ShaderProgram, shader);
	}
}

bool GLSLShader::IsLinked() const
{
	return linked;
}

void GLSLShader::Link() 
{
	if (linked) return;
	if (ShaderProgram <= 0)
	{
		throw GLSLShaderExcepton("Creating Shader Program fail");
	}
	glLinkProgram(ShaderProgram);

	int status = 0;
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &status);
	if (status == GL_FALSE) 
	{
		int length = 0;
		std::string logString;
		glGetProgramiv(ShaderProgram, GL_INFO_LOG_LENGTH, &length);

		if (length > 0) 
		{
			char* c_log = new char[length];
			int written = 0;
			glGetProgramInfoLog(ShaderProgram, length, &written, c_log);
			logString = c_log;
			delete[] c_log;
		}
		throw GLSLShaderExcepton("Shader Program link failed. " + logString);
	}
	else 
	{
		linked = true;
	}
}

void GLSLShader::Use() 
{
	glUseProgram(ShaderProgram);
}