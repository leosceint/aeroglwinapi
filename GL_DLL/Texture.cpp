#include "Texture.h"

Texture::Texture() 
{
	TexID = 0;
}

GLuint Texture::GetTexID() const 
{
	return TexID;
}

void Texture::SetTexID(GLuint id) 
{
	TexID = id;
}

void Texture::LoadImage(std::string Path)
{
	int Width;
	int Height;
	unsigned char* Image = SOIL_load_image(Path.c_str(), &Width, &Height, 0, SOIL_LOAD_RGBA);
	if (Image == nullptr) 
	{
		throw TextureException("Load Image for texture " + Path + " fail");
	}
	else
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, Image);
		SOIL_free_image_data(Image);
	}
}

GLuint Texture::LoadTexture(std::string Path) 
{
	GLuint id;
	glGenTextures(1, &id);
	SetTexID(id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	LoadImage(Path);
	glBindTexture(GL_TEXTURE_2D, 0);
	return id;
}