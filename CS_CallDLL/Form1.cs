﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace CS_CallDLL
{
    public partial class Form1 : Form
    {
        const string dllname = "GL_DLL.dll";
        [DllImport(dllname)]
        private static extern int LIBIUP_Main(int width, int height);
        private IntPtr Hdc;

        public Form1(int width, int x, int y)
        {

            InitializeComponent();
            this.Location= new Point(x, y);
            //LIBIUP_Main(1000, 822);
            
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            LIBIUP_Main(1000, 822);
        }
    }
}
