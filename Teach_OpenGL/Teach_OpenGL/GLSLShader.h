#ifndef GLSLSHADER_H
#define GLSLSHADER_H
#include "OpenGL.h"
#include <stdexcept>

namespace GLSL {

	const char PreInit_VertexShader[] =
		"#version 400\n"
		"layout (location = 0) in vec3 vp;"
		"layout (location = 1) in vec2 inTexCoord;"
		"out vec2 outTexCoord;"
		"void main() {"
		"  gl_Position = vec4(vp, 1.0);"
		"  outTexCoord = inTexCoord;"
		"}";

	const char PreInit_FragmentShader[] =
		"#version 400\n"
		"in vec2 outTexCoord;"
		"out vec4 frag_color;"
		"uniform sampler2D frame;"
		"void main() {"
		"  vec4 color = texture(frame, outTexCoord);"
		"  frag_color = color;"
		"}";
}

class GLSLShaderExcepton : public std::runtime_error 
{
public:
	GLSLShaderExcepton(const std::string& msg) : std::runtime_error(msg)
	{}
};

class GLSLShader 
{
public:
	GLSLShader();
	~GLSLShader();
	GLuint GetShaderProgram() const { return ShaderProgram; };
	//� �������� ��� ���������������� ��������
	//��. GLSL namespace
	void CompileShader(const char* shader_code, int Shader_Type);
	void Link();
	void Use();
	bool IsLinked() const;

private:	
	GLuint ShaderProgram;
	bool linked;
};
#endif