#ifndef TEXTURE_H
#define TEXTURE_H
#include "OpenGL.h"
#include "SOIL.h"
#include <string>
#include <stdexcept>

class TextureException : public std::runtime_error 
{
public:
	TextureException(const std::string& msg): std::runtime_error(msg)
	{}
};

class Texture 
{
public:
	Texture();
	GLuint	LoadTexture(std::string Path);
	GLuint	GetTexID() const;
	void	SetTexID(GLuint id);

private:
	void LoadImage(std::string Path);

private:
	GLuint TexID;
};
#endif