#ifndef GLWINDOW_H
#define GLWINDOW_H

//#define _WIN32_WINNT 0x0500

#include <windows.h>
#include <dwmapi.h>
//=================================
//����� ����
//=================================
class GLWindow 
{
public:
	GLWindow(LPCTSTR WindowName, int Width, int Height,
		LRESULT (WINAPI *pWndProc)(HWND, UINT, WPARAM, LPARAM));
	HWND GetHWND() const;
	HDC GetHDC() const;
	RECT GetDesktopResolution() const;
	BOOL Show();
public:
	bool IsRunning;
protected:
	HWND hWnd;
	HDC hDC;
	WNDCLASSEX WindowClass;
};
#endif