#ifndef GLCONTEXT_H
#define GLCONTEXT_H
#include <windows.h>
#include "OpenGL.h"
#include "Texture.h"
#include "GLSLShader.h"
#include <sstream>
#include <string>
//

// HERE

typedef void(__cdecl* Page_MS21_HUD_Draw)(HDC hdc, int xLeft, int yTop, int height, float brightness, int* inputs, int* inputsFPL);

class GLContext
{
public:
    static GLContext& Instance()
    {
        static GLContext glc;
        return glc;
    }
    int InitHGLRC(HDC hDC, HGLRC* hGLRC, std::string version);
    void InitScene(int Width, int Height);
    void DrawScene();
    void DeInitHGLRC(HGLRC* hGLRC);
    void DetectVersion();

private:
    GLContext() : m_isInited(0) {}
    ~GLContext() {}
    GLContext(const GLContext&);
    GLContext& operator=(const GLContext&);

private:
    HMODULE m_scade;
    Page_MS21_HUD_Draw m_proc_addr;
    HDC m_hDC;
    //��������� OpenGL
    HGLRC m_scadeRC;//��� Scade � �����
    HGLRC m_frameRC;//��� �����

    GLuint vao;
    GLSLShader ShaderProgram;
    Texture HUD_Texture;

    char m_isInited;
};
#endif
