#ifndef TEXTURE_H
#define TEXTURE_H
#include "OpenGL.h"
#include "SOIL.h"
#include <string>

class Texture 
{
public:
	Texture();
	GLuint	LoadTexture(std::string Path);
	GLuint	GetTexID() const;
	void	SetTexID(GLuint id);

private:
	void LoadImage(std::string Path);

private:
	GLuint TexID;
};
#endif