#include "GLContext.h"
#include "Texture.h"
#include "GLSLShader.h"

float angle = 0.0f;

GLfloat points[] = {
	//x      y      z    //s      t
	1.0f,  1.0f,  0.0f,   1.0f, 0.0f,
	1.0f, -1.0f,  0.0f,   1.0f, 1.0f,
   -1.0f, -1.0f,  0.0f,   0.0f, 1.0f,
   -1.0f,  1.0f,  0.0f,   0.0f, 0.0f
};

GLuint indices[] = {
	0, 1, 3,
	1, 2, 3
};

GLuint vao;
GLSLShader ShaderProgram; 
Texture HUD_Texture;

int GLContext::InitHGLRC(HDC hDC, HGLRC* hGLRC, std::string version)
{
	int pos = version.find(".");
	std::istringstream iss(version.substr(0, pos) + ' ' + version.substr(pos + 1, version.length()), std::istringstream::in);
	int major; int minor;
	iss >> major >> minor;

	PIXELFORMATDESCRIPTOR		pfd;
	HGLRC						hRCtemp;
	int							format;

	PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = NULL;

	int GL_attribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, major,
		WGL_CONTEXT_MINOR_VERSION_ARB, minor,
		WGL_CONTEXT_FLAGS_ARB, 0,
		0
	};

	//�������� ������� ��������
	pfd = {
	  sizeof(PIXELFORMATDESCRIPTOR),
	  1,                                // Version Number
	  PFD_DRAW_TO_WINDOW |         // Format Must Support Window
	  PFD_SUPPORT_OPENGL |         // Format Must Support OpenGL
	  PFD_SUPPORT_COMPOSITION |         // Format Must Support Composition
	  PFD_DOUBLEBUFFER,                 // Must Support Double Buffering
	  PFD_TYPE_RGBA,                    // Request An RGBA Format
	  32,                               // Select Our Color Depth
	  0, 0, 0, 0, 0, 0,                 // Color Bits Ignored
	  8,                                // An Alpha Buffer
	  0,                                // Shift Bit Ignored
	  0,                                // No Accumulation Buffer
	  0, 0, 0, 0,                       // Accumulation Bits Ignored
	  32,                               // 16Bit Z-Buffer (Depth Buffer)
	  8,                                // Some Stencil Buffer
	  0,                                // No Auxiliary Buffer
	  PFD_MAIN_PLANE,                   // Main Drawing Layer
	  0,                                // Reserved
	  0, 0, 0                           // Layer Masks Ignored
	};

	format = ChoosePixelFormat(hDC, &pfd);
	if (!format || !SetPixelFormat(hDC, format, &pfd))
	{
		MessageBox(NULL, "Setting pixel format fail", "Error", MB_OK | MB_ICONERROR);
		return -1;
	}

	hRCtemp = wglCreateContext(hDC);
	if (!hRCtemp || !wglMakeCurrent(hDC, hRCtemp))
	{
		MessageBox(NULL, "�reating temp render context fail", "Error", MB_OK | MB_ICONERROR);
		return -1;
	}
	wglCreateContextAttribsARB = reinterpret_cast<PFNWGLCREATECONTEXTATTRIBSARBPROC>(
		wglGetProcAddress("wglCreateContextAttribsARB"));

	if (!wglCreateContextAttribsARB)
	{
		MessageBox(NULL, "wglCreateContextAttribsARB fail", "Error", MB_OK | MB_ICONERROR);
		return -1;
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(hRCtemp);

	*hGLRC = wglCreateContextAttribsARB(hDC, 0, GL_attribs);
	if (!(*hGLRC) || !wglMakeCurrent(hDC, *(hGLRC)))
	{
		MessageBox(NULL, "Creating render context fail", "Error", MB_OK | MB_ICONERROR);
		return -1;
	}
	m_frameRC = *hGLRC;
	m_scadeRC = wglCreateContextAttribsARB(hDC, 0, GL_attribs);

	GLenum error = glewInit();
	if (GLEW_OK != error)
	{
		MessageBox(NULL, "Glew Init - fail", "Error", MB_OK | MB_ICONERROR);
		return -1;
	}

	// SCADE
	m_scade = LoadLibrary("Page_MS21_HUD.dll");

	if (m_scade)
	{
		m_proc_addr = (Page_MS21_HUD_Draw)GetProcAddress(m_scade, "Page_MS21_HUD_Draw");

		//FreeLibrary(m_scade);
	}

	m_hDC = hDC;

	return 0;
}

//
void GLContext::InitScene(int Width, int Height)
{
	wglMakeCurrent(m_hDC, m_frameRC);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glShadeModel(GL_SMOOTH);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-1.0, 1.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);

	// Load textre
	HUD_Texture.LoadTexture("HUD.png");
	// 1. ���� ������ GLFloat �������/�������� -> � ������ GPU
	// vertices
	
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLuint vbo = 0;
	GLuint ebo = 0;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// 2. ����� ������ �� ���������� ������� ������ � �����-� ���������

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	//3. ������ ��� ��������, ���� �� �����

	try
	{
		// 4. ����������� �������
		ShaderProgram.CompileShader(GLSL::PreInit_VertexShader, GL_VERTEX_SHADER);
		ShaderProgram.CompileShader(GLSL::PreInit_FragmentShader, GL_FRAGMENT_SHADER);
		// 5. �������� ��������� ���������
		ShaderProgram.Link();
	}
	catch (GLSLShaderExcepton & err) 
	{
		MessageBox(NULL, err.what(), "Error", MB_OK | MB_ICONERROR);
		exit(EXIT_FAILURE);
	}
}
//
void GLContext::DrawScene()
{
	static float dx = 0.0;

	wglMakeCurrent(m_hDC, m_scadeRC);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	static int ShaderLoc = -1;
	if (ShaderLoc == -1)
	{
		glGetIntegerv(GL_CURRENT_PROGRAM, &ShaderLoc);
	}

	//-----------------------------------
	// OpenGL 2.0
	// ���� ���� SCADE + ����������� + SVS/EVS/CVS
	//-----------------------------------
	glUseProgram(ShaderLoc);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D, 0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glBegin(GL_TRIANGLES);

	dx += 0.01f;
	glColor4f(1.0f, 0.0f, 0.0f, 0.23f);
	glVertex3d(-0.5 + dx, -0.5, 0);
	glVertex3d(0.5 + dx, -0.5, 0);
	glVertex3d(0.0 + dx, 0.5, 0);

	if (dx > 1.0)
	{
		dx = 0.0;
	}

	glEnd();

	// TODO
	if (m_proc_addr)
	{
		//T_MS21_PFD_Inputs PFD;
		//T_MS21_ND_FPL_Inputs FPL;

		unsigned char PFD[2252];
		unsigned char FPL[49632];

		memset(&PFD, 0, sizeof(PFD));
		memset(&FPL, 0, sizeof(FPL));

		// (HDC hdc, int xLeft, int yTop, int height, float brightness, int* inputs, int* inputsFPL);
		m_proc_addr(m_hDC, 40, 0, 750, 1.0, reinterpret_cast<int*>(&PFD), reinterpret_cast<int*>(&FPL));
	}

	//LoadLibrary()

	wglMakeCurrent(m_hDC, NULL);

	wglMakeCurrent(m_hDC, m_frameRC);
	//-----------------------------------
	// OpenGL 3.3
	// �����
	//-----------------------------------
	//6. ����������� ��������� ���������
	ShaderProgram.Use();

	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);

	// ��������� � ���� ������ �����-�
	glBindTexture(GL_TEXTURE_2D, HUD_Texture.GetTexID());

	// �������� ��������� ������� ������
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	wglMakeCurrent(m_hDC, NULL);

	Sleep(15);
}
//
void GLContext::DeInitHGLRC(HGLRC* hGLRC)
{
	ChangeDisplaySettings(NULL, 0);
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(*hGLRC);
}
//
void GLContext::DetectVersion()
{
	int major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	std::ostringstream ss;
	ss << "\nVersion: " << major << "." << minor
		<< "\nRenderer: " << (const char*)glGetString(GL_RENDERER)
		<< "\nVendor: " << (const char*)glGetString(GL_VENDOR)
		<< "\nVersion2: " << (const char*)glGetString(GL_VERSION)
		<< "\nGLSL ver: " << (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
	std::string s = ss.str();
	MessageBox(NULL, s.c_str(), "Detected ver. Open GL", MB_OK);
}